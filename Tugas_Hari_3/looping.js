//Soal nomer 1
let increment = 2;
let position = 0;
let firstMessage = 'I love coding';
let secondMessage = 'I Will become a mobile developer';
let firstHeaderMessage = 'LOOPING PERTAMA';
let secondHeaderMessage = 'LOOPING KEDUA';
position = position + increment;
console.log(firstHeaderMessage);
while (position <= 20) {
    console.log(position + ' - ' + firstMessage);
    position = position + increment;
}
console.log(secondHeaderMessage);
position = position - increment;
while (position >= 2) {
    console.log(position + ' - ' + secondMessage);
    position = position - increment;
}

console.log('\n');
//Soal nomer 2
//angka ganjil santai
//angka genap berkualitas
//kelipatan 3 dan ganjil tampilkan i love coding
let message = "";
let limit = 30;
for (var angka = 1; angka <= limit; angka++) {
    message = angka;
    if (angka % 2 == 0) {
        message += ' - Berkualitas';
    } else {
        if (angka % 3 == 0) {
            message += ' - I Love coding';
        } else {
            message += ' - Santai';
        }
    }
    console.log(message);
}

console.log('\n');
//Soal nomer 3
let limitColumn = 8;
let limitRow1 = 4;
for (var row = 1; row <= limitRow1; row++) {
    let draw = "";
    for(var column = 1; column<= limitColumn; column++){
        draw += "#";
    }
    console.log(draw);
}

console.log('\n');
//Soal nomer 4
let limitRow2 = 7;
for (var row = 1; row <= limitRow2; row++) {
    let draw = '';
    for (var x = 1; x <= row; x++) {
        draw += '#';
    }
    console.log(draw);
}

console.log('\n');
//Soal nomer 5
//Membuat papan catur 8x8
let limitRow3 = 8;
let limitCol = 8;
for (var row = 1; row <= limitRow3; row++) {
    let draw = "";
    for (var col = 1; col <= limitCol; col++) {
        switch (row % 2) {
            case 0:
                if (col % 2 == 0) {
                    draw += ' ';
                } else {
                    draw += '#';
                }
                break;
            case 1:
                if (col % 2 == 0) {
                    draw += '#';
                } else {
                    draw += ' ';
                }
                break;
        }
    }
    console.log(draw);
}