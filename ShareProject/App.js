import React from 'react';
import { View, StyleSheet, Button, Text } from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Tugas12 from './Tugas/Tugas12/App';
import Tugas13Login from './Tugas/Tugas13/LoginScreen';
import Tugas13About from './Tugas/Tugas13/AboutScreen';
import Tugas13Registration from './Tugas/Tugas13/RegistrationScreen';
import Tugas14 from './Tugas/Tugas14/App';
import Tugas15 from './Tugas/Tugas15/App';

const Stack = createStackNavigator();

export default App = () => {
  return(
    // <Tugas13Login/>
    // <Tugas13About/>
    // <Tugas13Registration/>
    // <Tugas14/>
    <Tugas15/>
    // <NavigationContainer>
    //   <Stack.Navigator initialRouteName="Home">
    //     <Stack.Screen name="Home" component={HomeScreen} options={{headerShown:false}} />
    //     <Stack.Screen name="Details" component={DetailScreen} />
    //     <Stack.Screen name="Profiles" component={ProfileScreen} />
    //   </Stack.Navigator>
    // </NavigationContainer>
  );
}

HomeScreen=({navigation})=>{
  return(
    <View style={styles.container}>
      <Text>Home Screen</Text>
      <Button title="Go To Details"
              onPress={() => navigation.navigate('Details')}/>
      <Button title="Go To Profile"
              onPress={() => navigation.navigate('Profiles',{key: 'Enggar'})}/>
    </View>
  );
}


DetailScreen=({navigation})=>{
  return(
    <View style={styles.container}>
      <Text>Detail Screen</Text>
      <Button title="Go To Details" style={styles.button}
              onPress={()=> navigation.push('Details')}/>
      <Button title="Go To Home" style={styles.button}
              onPress={() => navigation.navigate('Home')}/>
      <Button title="Go Back" style={styles.button}
              onPress={() => navigation.goBack()}/>
      <Button title="Go Back To First Screen in Stack" style={styles.button}
              onPress={() => navigation.popToTop()}/>
    </View>
  );
}


ProfileScreen=({route ,navigation})=>{
  return(
    <View style={styles.container}>
      <Text>Detail Screen</Text>
      <Button title="Go To Home" style={styles.button}
              onPress={() => navigation.navigate('Home')}/>
      <Button title="Go Back" style={styles.button}
              onPress={() => navigation.goBack()}/>
      <Button title="Go Back To First Screen in Stack" style={styles.button}
              onPress={() => navigation.popToTop()}/>
      <Text>Params = {route.params.key}</Text>
    </View>
  );
}



const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  },
  button: {
    marginVertical:100
  }
});