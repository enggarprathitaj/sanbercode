import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Home, Details, Search, Search2, 
         Profile, Splash, SignIn, CreateAccount } from './Screen';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const Stack = createStackNavigator();
const TabsBottom = createBottomTabNavigator();

export default function App() {
  return (
    
      <NavigationContainer>
        <Stack.Navigator initialRouteName="SignIn">
          <Stack.Screen name="SignIn" component={SignIn}></Stack.Screen>
          <Stack.Screen name="CreateAccount" component={CreateAccount}></Stack.Screen>
          <Stack.Screen name="TabBottom" component={TabBottom}/>
        </Stack.Navigator>

      </NavigationContainer>
  );
}

const TabBottom=()=>{
  return(
    <TabsBottom.Navigator>
      <TabsBottom.Screen name="Home" component={Home}></TabsBottom.Screen>
      <TabsBottom.Screen name="Profile" component={Profile}></TabsBottom.Screen>
    </TabsBottom.Navigator>
  );
        
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});