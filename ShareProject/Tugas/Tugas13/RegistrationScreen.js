import React from 'react';
import { StyleSheet, Text, View, 
         ScrollView, Image, TouchableOpacity, 
         FlatList, TextInput, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default RegistrationScreen = () => {
    return (
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.logoSection}>
                    <Image source = {require('./images/logo.png')}/>
                </View>
                <View style={styles.titleSection}>
                    <Text style= {styles.titleScreen}>Register</Text>
                </View>
                <View style={styles.formSection}>
                    <Text style={styles.labelInput}>Username</Text>
                    <TextInput style={styles.inputTextForm}/>

                    <Text style={styles.labelInput}>Email</Text>
                    <TextInput style={styles.inputTextForm}/>

                    <Text style={styles.labelInput}>Password</Text>
                    <TextInput style={styles.inputTextForm}/>

                    <Text style={styles.labelInput}>Ulangi Password</Text>
                    <TextInput style={styles.inputTextForm}/>

                    <View style={styles.buttonSection}>
                        <Button title='Masuk' color="#3EC6FF" style={styles.buttonItem}/>
                        <Text style={{textAlign:'center', 
                                      marginBottom: 20, 
                                      marginTop:20}}>atau</Text>
                        <Button title='Daftar' style= {styles.buttonItem} color="#003366"/>
                    </View>

                </View>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        backgroundColor: '#FFFFFF',
        paddingVertical:20,
        
    },
    logoSection : {
        alignItems:'center'
    },
    titleSection: {
        alignItems: 'center',
        paddingVertical:20
    },
    titleScreen : {
        color: '#003366',
        fontSize: 24,
        fontFamily: 'Roboto',
    },
    formSection: {
        color: '#003366',
        fontSize: 16,
        fontFamily: 'Roboto',
        flexDirection: 'column',
        paddingVertical:20
    },
    labelInput: {
        left:41,
        marginBottom:5
    },
    inputTextForm: {
        height: 40,
        borderColor:'gray',
        borderWidth:1,
        marginBottom:20,
        marginHorizontal:41
        
    },
    buttonSection : {
        flexDirection:'column',
        paddingVertical:20,
        borderRadius:20,
        paddingHorizontal:40
    },
    buttonItem: {
        marginHorizontal:50,
        borderRadius:20,
        alignItems:'center'
        
    }

});