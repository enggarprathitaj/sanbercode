import React from 'react';
import { StyleSheet, Text, View, Button, Image, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const profile = {
    name: 'R. Enggar Prathita Joharnovta',
    position: 'Jr. React Native Developer',
    gitlab: '@enggarprathitaj',
    github: '@enggarprdh',
    facebook: 'Catatan Programmer',
    instagram: '@enggarprathita',
    twitter: '@pejuangbubur'
};

export default AboutScreen = () => {
    return(
        <ScrollView>
            <View style={styles.container}>
                
                <View style={styles.topSection}>
                <Text style={styles.title}>Tentang Saya</Text>
                <View style={styles.profilePicture}>
                    <Image source={require('../Tugas13/images/Ellipse.png')}/>
                    <Image source={require('../Tugas13/images/Vector.png')}
                        style={{bottom:172, left:36}}/>
                </View>
                <View style={styles.detailProfile}>
                    <Text style={styles.name}>{profile.name}</Text>
                    <Text style={styles.position}>{profile.position}</Text>
                </View>
            </View>
                <View style={styles.middleSection}>
                    <Text style={styles.titleDetail}>Portofolio</Text>
                    <View style={styles.line}></View>
                    <View style={styles.portoContent}>
                        <View style={styles.portoItem}>
                            <FontAwesome style={styles.iconSocialMedia} name="github"/>
                            <Text style={styles.portoTitleItem}>{profile.github}</Text>
                        </View>
                        <View style={styles.portoItem}>
                            <FontAwesome style={styles.iconSocialMedia} name="gitlab"/>
                            <Text style={styles.portoTitleItem}>{profile.gitlab}</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.bottomSection}>
                    <Text style={styles.titleDetail}>Hubungi Saya</Text>
                    <View style={styles.line}></View>
                    <View style={styles.contactContent}>
                        <View style={styles.contactContentItem}>
                            <FontAwesome style={styles.iconSocialMedia} name="instagram"/>
                            <Text style={styles.titleSocial}>{profile.instagram}</Text>
                        </View>
                        <View style={styles.contactContentItem}>
                            <FontAwesome style={styles.iconSocialMedia} name="youtube"/>
                            <Text style={styles.titleSocial}>{profile.facebook}</Text>
                        </View>
                        <View style={styles.contactContentItem}>
                            <FontAwesome style={styles.iconSocialMedia} name="twitter"/>
                            <Text style={styles.titleSocial}>{profile.twitter}</Text>
                        </View>
                    </View>
                    
                </View>
                
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        flexDirection:'column'
    },
    topSection: {
        alignItems:'center',
        top:64,
        flexDirection:'column'
    },
    title: {
        fontSize:36,
        color:'#003366'
    },
    profilePicture: {
        marginTop:20
    },
    detailProfile: {
        bottom:115,
        flexDirection:'column',
        alignItems:'center'
    },
    name:{
        fontSize:20,
        fontWeight:'bold',
        marginBottom:5,
        color:'#003366'
    },
    position:{
        fontSize:18,
        fontWeight:'bold',
        color:'#3EC6FF'
    },
    middleSection: {
        backgroundColor:'#EFEFEF',
        height:140,
        bottom: 30,
        borderRadius:10,
        marginHorizontal:20,
        paddingHorizontal:15,
        paddingVertical:5,
        marginBottom:20
    },
    portoContent:{
        flexDirection:'row',
        marginHorizontal:30,
        paddingVertical:20
    },
    portoItem : {
        flex:6,
        flexDirection:'column',
        marginHorizontal:10,
        alignItems:'center'
    },
    portoTitleItem: {
        color: '#003366',
        marginTop:10
    },    
    titleDetail: {
        color: '#003366',
        fontSize:18,
        marginBottom:5
    },
    line: {
        height:2,
        backgroundColor:'#003366'
    },
    bottomSection: {
        backgroundColor:'#EFEFEF',
        bottom: 30,
        borderRadius:10,
        paddingHorizontal:15,
        paddingVertical:5,
        marginHorizontal:20,
    },
    iconSocialMedia : {
        color:'#3EC6FF',
        fontSize:40,
    },
    contactContent: {        
        flexDirection:'column',
        padding:20,
    },
    contactContentItem: {
        flexDirection:'row',
        marginHorizontal:60
    }, 
    titleSocial: {
        marginTop:10,
        color: '#003366',
        marginHorizontal:20
    }
    

})