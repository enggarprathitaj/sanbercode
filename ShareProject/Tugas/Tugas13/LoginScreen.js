import React from 'react';
import { StyleSheet, Text, View, 
         ScrollView, Image, TouchableOpacity, 
         FlatList, TextInput, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default LoginScreen = () => {
    return (
        <ScrollView>
        <View style={styles.container}>
            <View style={styles.logoSection}>
                <Image source = {require('./images/logo.png')}/>
            </View>
            <View style={styles.titleSection}>
                <Text style= {styles.titleScreen}>Login</Text>
            </View>
            <View style={styles.formSection}>
                <Text style={styles.labelInput}>Username / Email</Text>
                <TextInput style={styles.inputTextForm}/>

                <Text style={styles.labelInput}>Password</Text>
                <TextInput style={styles.inputTextForm}/>

                <View style={styles.buttonSection}>
                    <Button title='Masuk' color="#3EC6FF" style={styles.buttonItem}/>
                    <Text style={{textAlign:'center', 
                                  marginBottom: 20, 
                                  marginTop:20}}>atau</Text>
                    <Button title='Daftar' color="#003366" style={styles.buttonItem}/>
                </View>
                
            </View>
        </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        backgroundColor: '#FFFFFF',
        paddingVertical:20
    },
    logoSection : {
        alignItems:'center'
    },
    titleSection: {
        alignItems: 'center',
        height:30,
        marginVertical:40
    },
    titleScreen : {
        color: '#003366',
        fontSize: 24,
        fontFamily: 'Roboto-Regular',
    },
    formSection: {
        color: '#003366',
        fontSize: 16,
        fontFamily: 'Roboto',
        flexDirection: 'column',
    },
    labelInput: {
        left:41,
        marginBottom:5,
        fontFamily: 'Roboto-Regular',
    },
    inputTextForm: {
        height: 40,
        borderColor:'gray',
        borderWidth:1,
        marginHorizontal:41,
        marginBottom:20,
        fontFamily: 'Roboto-Regular',
    },
    buttonSection : {
        flexDirection:'column',
        paddingHorizontal:40,
    },
    buttonItem: {
        marginHorizontal:40,
        fontFamily: 'Roboto-Regular',
    }

});