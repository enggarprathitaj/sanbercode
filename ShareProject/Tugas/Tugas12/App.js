import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './components/videoItem';
import data from './data.json';

export default App = () => {
    return (
        <View style={styles.container}>
            <View style = {styles.navBar}>
                <Image source = {require('./images/logo.png')} style={styles.navImage} />
                <View style = {styles.rightNav}>
                    <TouchableOpacity>
                        <Icon style={styles.navItem} name="search" size={25} color='#757575'/>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon style={styles.navItem} name="account-circle" size={25} color='#757575'/>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.body}>
                <FlatList data= {data.items} 
                          renderItem = {(video)=><VideoItem video ={video.item}></VideoItem>}
                          keyExtractor = {(item) => item.id}
                          ItemSeparatorComponent = {()=> <View style={{height:0.5, backgroundColor:'#CCCCCC'}}></View>}
                />
            </View>
            <View style = {styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="home" size={25} color='#757575'/>
                        <Text style={styles.tabTitle}>Home</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="explore" size={25} color='#757575'/>
                        <Text style={styles.tabTitle}>Explore</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.tabItemCenter}>
                        <Icon name="control-point" size={50} color='#757575'/>
                    </TouchableOpacity>


                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="subscriptions" size={25} color='#757575'/>
                        <Text style={styles.tabTitle}>Subscriptions</Text>
                    </TouchableOpacity>

                    
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="video-library" size={25} color='#757575'/>
                        <Text style={styles.tabTitle}>Library</Text>
                    </TouchableOpacity>
            </View>
        </View>
    ); 
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    navImage: {
        width:100,
        height:22
    },
    navBar: {
        height: 55,
        backgroundColor: '#fff',
        elevation:3,
        paddingHorizontal:15,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        
    },
    rightNav: {
        flexDirection:'row'
    },
    navItem: {
        marginLeft:25,
    },
    body:{
        flex:1
    },
    tabBar: {
        backgroundColor:'#fff',
        height:55,
        borderTopWidth:0.5,
        borderColor:'#e5e5e5',
        flexDirection:'row',
        justifyContent: 'space-around'
    },
    tabItem: {
        alignItems:'center',
        justifyContent:'center',
        padding:10,
    },
    tabItemCenter: {
        alignItems:'center',
        justifyContent:'center',
        left:10,
        bottom:5
        
    },
    tabTitle: {
        fontSize:10,
        color:"#3c3c3c"
    }
});

