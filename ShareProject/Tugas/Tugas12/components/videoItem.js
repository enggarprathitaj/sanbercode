import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class VideoItem extends Component {
    render=() => {
        let video = this.props.video;
        return (
            <View style={styles.container}>
                <TouchableOpacity>
                    <Image source={{uri: video.snippet.thumbnails.medium.url}} style={{height:200}}></Image>
                </TouchableOpacity>
                <View style={styles.descContainer}>
                    <TouchableOpacity>
                        <Image source={{uri: 'https://randomuser.me/api/portraits/men/13.jpg'}} 
                               style={{width:50,height:50, borderRadius:25}}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.videoDetails}>
                            <Text style={styles.videoTitle}>{video.snippet.title}</Text>
                            <Text style={styles.videoStats}>{video.snippet.channelTitle + ' • ' + 
                                    formatNumber(video.statistics.viewCount,1) + ' • 3 months ago'}</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon name='more-vert' size={20} color='#999999'></Icon>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }    
}

formatNumber = (num, digits) => {
    let word = [
        { value: 1, symbol: "" },
        { value: 1E3, symbol: "k" },
        { value: 1E6, symbol: "M" },
        { value: 1E9, symbol: "G" },
        { value: 1E12, symbol: "T" },
        { value: 1E15, symbol: "P" },
        { value: 1E18, symbol: "E" }
    ];
    let regex = /\.0+$|(\.[0-9]*[1-9])0+$/;
    let i;
    for(i = word.length -1; i > 0; i--){
        if(num >= word[i].value) {
            break;
        }
    }
    return (num / word[i].value).toFixed(digits).replace(regex, '$1') + word[i].symbol + ' views';
}


const styles = StyleSheet.create({
    container: {
        padding:15
    },
    descContainer: {
        flexDirection:'row',
        paddingTop:15
    },
    videoDetails : {
        paddingHorizontal:15,
        flex:1
    },
    videoTitle: {
        fontSize:16,
        color: '#3c3c3c',
    },
    videoStats: {
        fontSize:12,
        color:'#757575',
        paddingTop:3
    }
    
})
