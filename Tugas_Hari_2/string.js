//Soal nomer 1
let word = 'Javascript';
let second = 'is';
let third = 'awesome';
let fourth = 'and';
let fifth = 'I';
let sixth = 'love';
let seventh = 'it!';
let concatStr = word + ' ' + second + ' ' + ' ' + third + ' ' + fourth + ' ' + ' ' + fifth + ' ' + sixth + ' ' + seventh;
console.log(concatStr);
console.log("\n");

//Soal nomer 2
let sentences = 'I am going to be React Native Developer';
let splitSentences = sentences.split(' ');
let firstWord = splitSentences[0];
let secondWord = splitSentences[1];
let thirdWord = splitSentences[2];
let fourthWord = splitSentences[3];
let fifthWord = splitSentences[4];
let sixthWord = splitSentences[5];
let seventhWord = splitSentences[6];
let eighthWord = splitSentences[7];
console.log('First Word: ' + firstWord);
console.log('Second Word: ' + secondWord);
console.log('Third Word: ' + thirdWord);
console.log('Fourth Word: ' + fourthWord);
console.log('Fifth Word: ' + fifthWord);
console.log('Sixth Word: ' + sixthWord);
console.log('Seventh Word: ' + seventhWord);
console.log('Eighth Word: ' + eighthWord)
console.log("\n");

//Soal nomer 3
let sentences2 = 'wow JavaScript is so cool';
let firstWord2 = sentences2.substring(0, 3);
let secondWord2 = sentences2.substring(4, 14);
let thirdWord2 = sentences2.substring(15, 17);
let fourthWord2 = sentences2.substring(18, 20);
let fifthWord2 = sentences2.substring(21, 25);
console.log('First Word: ' + firstWord2);
console.log('Second Word: ' + secondWord2);
console.log('Third Word: ' + thirdWord2);
console.log('Fourth Word: ' + fourthWord2);
console.log('Fifth Word: ' + fifthWord2);
console.log("\n");

//Soal nomer 4
let sentences3 = 'wow JavaScript is so cool';
let firstWord3 = sentences3.substring(0, 3);
let secondWord3 = sentences3.substring(4, 14);
let thirdWord3 = sentences3.substring(15, 17);
let fourthWord3 = sentences3.substring(18, 20);
let fifthWord3 = sentences3.substring(21, 25);
console.log('First Word: ' + firstWord3 + ', with length: ' + firstWord3.length); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWord3.length); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWord3.length); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWord3.length); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWord3.length); 


